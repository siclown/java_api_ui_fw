# Java_API_UI_FW

A demo project for UI tests using Java, JUnit, Selenium WebDriver, RestAssured

## Overview
Java based.

Selenium WebDriver used for UI tests.

RestAssured used for API tests.

POM used for both UI and API test. 

Maven is used for build automation tool.

jUnit is used for testing framework.

ExtentReports used for reporting.

CI/CD enabled in Gitlab.

## Usage

Locally you'd need to have maven installed. And run **-mvn compile** in the local repo.

Reports are generated only when running **-mvn test**
