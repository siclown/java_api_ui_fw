package APIObjects.Endpoints;

import APIObjects.APIKeys;
import APIObjects.Request;
import APIObjects.URLs;

public class GetCurrentWeather extends Request {

    public GetCurrentWeather() {
        requestSpecification.basePath(URLs.getGetCurrentWeather());
    }

    public void addApiKey() {
        requestSpecification.queryParam("appid", APIKeys.getOpenWeatherKey());
    }
}
