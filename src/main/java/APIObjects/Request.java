package APIObjects;

import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.response.ResponseOptions;
import io.restassured.specification.RequestSpecification;

import java.util.LinkedHashMap;

public class Request {

    protected static RequestSpecification requestSpecification;
    private static final String baseUri = "https://api.openweathermap.org";

    public Request() {
        RequestSpecBuilder builder = new RequestSpecBuilder();
        builder.setBaseUri(baseUri);
        builder.setContentType(ContentType.JSON);
        RequestSpecification requestSpec = builder.build();
        requestSpecification = RestAssured.given().spec(requestSpec);
    }

    public APIObjects.Response get() {
        requestSpecification.log().all();
        ResponseOptions<Response> responseOptions = requestSpecification.get();
        responseOptions.getBody().print();
        APIObjects.Response response = new APIObjects.Response();
        response.statusCode = responseOptions.getStatusCode();
        response.body = responseOptions.getBody().as(LinkedHashMap.class);
        return response;
    }

    public void addQueryParameters(String paramIdentificator, String... strings) {
        requestSpecification.queryParam(paramIdentificator, strings);
    }
}
