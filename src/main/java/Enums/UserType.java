package Enums;

public enum UserType {
    BASE ("baseUser");

    public final String value;

    UserType(String value) {
        this.value = value;
    }
}
