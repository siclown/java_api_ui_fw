package PageObjects;

import Utils.Wait;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class ArticlePage extends BasePage{

    private Wait wait;

    private By articleTitle             = By.cssSelector(".article-page h1");
    public Header header                = new Header();

    //WebElements
    private WebElement getArticleTitle()            {return driver.findElement(articleTitle);}

    public ArticlePage() {
        this.wait = new Wait(driver, 6,2);
        wait.forVisibilityOf(getArticleTitle());
    }
}
