package PageObjects;

import Utils.Wait;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class Header extends BasePage{
    private Wait wait;

    private By conduitLogo           = By.linkText("conduit");
    private By settingsTab           = By.cssSelector("a[href='#settings']");
    private By signInTab             = By.cssSelector("a[href='#login']");
    private By newPostTab            = By.cssSelector("a[href='#editor']");
    private By homeTab               = By.cssSelector("a[href='#']");

    //WebElements
    private WebElement getConduitLogo()     {return driver.findElement(conduitLogo);}
    private WebElement getSettingsTab()     {return driver.findElement(settingsTab);}
    private WebElement getSignInTab()       {return driver.findElement(signInTab);}
    private WebElement getNewPostTab()      {return driver.findElement(newPostTab);}
    private WebElement getHomeTab()         {return driver.findElement(homeTab);}

    public Header() {
        this.wait = new Wait(driver, 6,2);
        wait.forVisibilityOf(getConduitLogo());
    }

    //clickers
    public void clickSettingsTab() {
        getSettingsTab().click();
    }

    public void clickSignInTab() {
        getSignInTab().click();
    }

    public void clickNewPostTab() {
        getNewPostTab().click();
    }

    public void clickHomeTab() {
        getHomeTab().click();
    }
}
