package PageObjects;

import Utils.Wait;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.List;

public class HomePage extends BasePage{

    private Wait wait;

    private By yourFeedTabTitle                          = By.linkText("Your Feed");
    private By globalFeedTabTitle                        = By.linkText("Global Feed");
    private By listOfArticles                            = By.cssSelector(".article-preview");
    private String userSpecificXpath(String userName)      {return String.format("((//div[@class='article-preview']//a[text()='%s'])[1])/ancestor::div[@class='article-preview']", userName);}
    public Header header                                 = new Header();

    //WebElements
    private WebElement getYourFeedTabTitle()        {return driver.findElement(yourFeedTabTitle);}
    private WebElement getGlobalFeedTitle()         {return driver.findElement(globalFeedTabTitle);}
    private List<WebElement> getListOfArticles()    {return driver.findElements(listOfArticles);}
    private WebElement getFirstArticleTitle(String userName) {
        String xpath = userSpecificXpath(userName) + "//h1";
        return driver.findElement(By.xpath(xpath));
    }
    private WebElement getFirstArticleSubject(String userName) {
        String xpath = userSpecificXpath(userName) + "//p";
        return driver.findElement(By.xpath(xpath));
    }

    public HomePage() {
        this.wait = new Wait(driver, 6,2);
        wait.forVisibilityOf(getYourFeedTabTitle());
        wait.staticSleep(1500);
        System.out.println("Navigated to Home page.");
    }

    //getters
    public String getFirstArticleTitleValue(String userName) {
        return getFirstArticleTitle(userName).getText();
    }

    public String getFirstArticleSubjectValue(String userName) {
        return getFirstArticleSubject(userName).getText();
    }

    //booleans
    public boolean isGetYourFeedTabTitleDisplayed() {
        return getYourFeedTabTitle().isDisplayed();
    }

    //clickers
    public void clickOnGlobalFeedTab() {
        getGlobalFeedTitle().click();
        wait.forVisibilityOf(getListOfArticles().get(0));
    }
}
