package PageObjects;

import Utils.Wait;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class LoginPage extends BasePage{

    private Wait wait;

    private By header           = By.cssSelector("h1");
    private By usernameInput    = By.cssSelector("input[type=email]");
    private By passwordInput    = By.cssSelector("input[type=password]");
    private By signInBtn        = By.cssSelector("button[type=submit]");

    //WebElements
    private WebElement getHeader()          {return driver.findElement(header);}
    private WebElement getUsernameInput()   {return driver.findElement(usernameInput);}
    private WebElement getPasswordInput()   {return driver.findElement(passwordInput);}
    private WebElement getSignInBtn()       {return driver.findElement(signInBtn);}

    public LoginPage() {
        this.wait = new Wait(driver, 6,2);
        wait.forTextToBe(getHeader(), "Sign In");
    }

    //booleans
    public boolean isLoginPage() {
        return getHeader().getText().equals("Sign In");
    }

    //setters
    public void setUsername(String username) {
        getUsernameInput().clear();
        getUsernameInput().sendKeys(username);
    }

    public void setPassword(String password) {
        getPasswordInput().clear();
        getPasswordInput().sendKeys(password);
    }

    //clickers
    public void clickSignInBtn() {
        getSignInBtn().click();
    }
}
