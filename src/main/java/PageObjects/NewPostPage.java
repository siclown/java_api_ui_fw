package PageObjects;

import Utils.Wait;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class NewPostPage extends BasePage{

    private Wait wait;

    private By publishArticleBtn        = By.cssSelector(".editor-page button");
    private By articleTitleField        = By.cssSelector("input[placeholder='Article Title']");
    private By whatsThisAboutField      = By.cssSelector("input[placeholder='What\\'s this article about?']");
    private By writeYourArticleField    = By.cssSelector("textarea[placeholder='Write your article (in markdown)']");
    public Header header                = new Header();

    //WebElements
    private WebElement getPublishArticleBtn()       {return driver.findElement(publishArticleBtn);}
    private WebElement getArticleTitleField()       {return driver.findElement(articleTitleField);}
    private WebElement getWhatsThisAboutField()     {return driver.findElement(whatsThisAboutField);}
    private WebElement getWriteYourArticleField()   {return driver.findElement(writeYourArticleField);}

    public NewPostPage() {
        this.wait = new Wait(driver, 6,2);
        wait.forVisibilityOf(getPublishArticleBtn());
    }

    //setters
    public void setArticleTitle(String articleTitle) {
        getArticleTitleField().clear();
        getArticleTitleField().sendKeys(articleTitle);
    }

    public void setWhatsThisAbout(String whatsThisAbout) {
        getWhatsThisAboutField().clear();
        getWhatsThisAboutField().sendKeys(whatsThisAbout);
    }

    public void setWriteYourArticle(String writeYourArticle) {
        getWriteYourArticleField().clear();
        getWriteYourArticleField().sendKeys(writeYourArticle);
    }

    //clickers
    public void clickPublishArticleBtn() {
        scrollIntoView(getPublishArticleBtn());
        getPublishArticleBtn().click();
    }
}
