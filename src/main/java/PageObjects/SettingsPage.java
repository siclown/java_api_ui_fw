package PageObjects;

import Utils.Wait;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class SettingsPage extends BasePage{
    private Wait wait;

    private By logo                = By.cssSelector("h1");
    private By logoutBtn           = By.cssSelector(".btn.btn-outline-danger");
    public Header header           = new Header();

    //WebElements
    private WebElement getLogo()        {return driver.findElement(logo);}
    private WebElement getLogoutBtn()   {return driver.findElement(logoutBtn);}

    public SettingsPage() {
        this.wait = new Wait(driver, 6,2);
        wait.forTextToBe(getLogo(), "Your Settings");
    }

    //clickers
    public void clickLogoutBtn() {
        scrollIntoView(getLogoutBtn());
        getLogoutBtn().click();
    }
}
