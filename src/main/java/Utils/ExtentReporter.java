package Utils;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.reporter.ExtentSparkReporter;
import com.aventstack.extentreports.reporter.configuration.Theme;

public class ExtentReporter {

    static ExtentReports report;

    public static ExtentReports generateReport() {
        ExtentSparkReporter spark = new ExtentSparkReporter("test-output/SparkReport.html");
        report = new ExtentReports();
        report.attachReporter(spark);
        report.setSystemInfo("OS", System.getProperty("os.name"));
        report.setSystemInfo("OS Version", System.getProperty("os.version"));
        spark.config().setTheme(Theme.DARK);
        spark.config().setDocumentTitle("Spark report");
        spark.config().setReportName("Demo tests results");

        return report;
    }
}
