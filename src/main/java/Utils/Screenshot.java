package Utils;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

import java.io.File;
import java.io.IOException;

public class Screenshot {

    public static String takeScreenshot(WebDriver driver, String methodName) throws IOException {
        String fileName = createName(methodName);
        String path = "test-output/" + fileName;
        File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
        File targetFile = new File(path);
        FileUtils.copyFile(scrFile, targetFile);

        return fileName;
    }

    private static String createName(String methodName) {
        String timestamp = StringUtils.timestamp();
        String fileName = methodName;
        fileName += timestamp + ".png";
        return fileName;
    }
}
