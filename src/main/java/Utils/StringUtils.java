package Utils;

import java.time.LocalDateTime;

public class StringUtils {

    public static String timestamp() {
        LocalDateTime timestamp = LocalDateTime.now();
        return timestamp.toString().replaceAll("[:T.-]", "").substring(0, 16);
    }
}
