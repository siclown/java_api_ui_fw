package Utils;

import Enums.UserType;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;

public class User{

    private final String FILE_PATH =  "src/main/resources/users.json";
    private ObjectMapper objectMapper = new ObjectMapper();
    private HashMap<String, HashMap<String, String>> usersFile;

    public String username;
    public String password;
    public String name;

    {
        try {
            usersFile = objectMapper.readValue(new File(FILE_PATH), HashMap.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void setToUser(UserType type) {
        HashMap<String, String> currentUser;
        currentUser = usersFile.get(type.value);
        this.username = currentUser.get("username");
        this.password = currentUser.get("password");
        this.name = currentUser.get("name");

        System.out.printf("Current user is: %s%n", type.value);
    }
}
