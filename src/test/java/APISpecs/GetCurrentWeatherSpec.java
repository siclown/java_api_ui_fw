package APISpecs;

import APIObjects.Endpoints.GetCurrentWeather;
import APIObjects.Response;
import Base.BaseApiSpec;
import org.junit.Assert;
import org.junit.Test;

public class GetCurrentWeatherSpec extends BaseApiSpec {

    String city = "New York";

    @Test
    public void getCurrentWeatherForLocationByCity() {
        GetCurrentWeather getCurrentWeather = new GetCurrentWeather();
        getCurrentWeather.addQueryParameters("q", city);
        getCurrentWeather.addApiKey();
        Response response = getCurrentWeather.get();

        Assert.assertEquals(200, response.statusCode);
        Assert.assertEquals(city, response.body.get("name"));
    }

    @Test
    public void unauthorizedUserDoesNotHaveAccess() {
        GetCurrentWeather getCurrentWeather = new GetCurrentWeather();
        getCurrentWeather.addQueryParameters("q", city);
        Response response = getCurrentWeather.get();

        Assert.assertEquals(401, response.statusCode);
    }
}
