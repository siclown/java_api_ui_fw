package Base;

import PageObjects.BasePage;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import java.util.concurrent.TimeUnit;

public class BaseUISpec {

    private final static int TIMEOUT = 5;
    private final static int PAGE_LOAD_TIMEOUT = 10;
    protected static WebDriver driver;

    @BeforeClass
    public static void beforeClass() {
        String osName = System.getProperty("os.name");

        WebDriverManager.chromedriver().setup();
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--start-maximized");

        if(osName.equalsIgnoreCase("Linux")) {
            options.setHeadless(true);
        }

        driver= new ChromeDriver(options);
        Listeners.driver = driver;
        System.out.println("Driver initialized.");
        driver.manage().timeouts().implicitlyWait(TIMEOUT, TimeUnit.SECONDS);
        driver.manage().timeouts().pageLoadTimeout(PAGE_LOAD_TIMEOUT, TimeUnit.SECONDS);
    }

    @Before
    public void before() {
        BasePage.driver = driver;
        driver.get(BasePage.BASE_URL);
        System.out.println("Navigated to Login page.");
    }

    @After
    public void after() {
        driver.manage().deleteAllCookies();
    }

    @AfterClass
    public static void afterClass() {
        driver.quit();
        System.out.println("Driver is down.");
    }
}
