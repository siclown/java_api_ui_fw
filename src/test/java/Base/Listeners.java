package Base;

import Utils.ExtentReporter;
import Utils.Screenshot;
import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.model.Media;
import org.junit.runner.Description;
import org.junit.runner.notification.Failure;
import org.junit.runner.notification.RunListener;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import java.io.IOException;

public class Listeners extends RunListener {

    public static WebDriver driver;
    private ExtentReports report = ExtentReporter.generateReport();
    private ExtentTest test;
    private String browser;
    private String browserVersion;


    public void testStarted(Description description) {
        test = report.createTest(description.getClassName() + "." + description.getMethodName());
        if(driver != null) {
            browser = ((RemoteWebDriver) driver).getCapabilities().getBrowserName();
            browserVersion = ((RemoteWebDriver) driver).getCapabilities().getVersion();
        }
    }

    public void testFinished(Description description) {
        report.flush();
    }

    public void testFailure(Failure failure) throws IOException {
        if (driver != null) {
            String screenshotPath = Screenshot.takeScreenshot(driver, failure.getDescription().getMethodName());
            Media screenshot = MediaEntityBuilder.createScreenCaptureFromPath(screenshotPath).build();

            test.fail(failure.getException(), screenshot);
        } else {
            test.fail(failure.getException());
        }
    }

    public void testSuiteFinished(Description description) {
        if (driver != null) {
            report.setSystemInfo("Browser", browser);
            report.setSystemInfo("Browser Version", browserVersion);
        }
    }
}
