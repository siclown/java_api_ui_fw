package UISpecs;

import Base.BaseUISpec;
import Enums.UserType;
import PageObjects.SettingsPage;
import Utils.User;
import PageObjects.HomePage;
import PageObjects.LoginPage;
import org.junit.Assert;
import org.junit.Test;

public class LoginSpec extends BaseUISpec {

    @Test
    public void verifyValidUserCanLogIn() {
        User user = new User();
        user.setToUser(UserType.BASE);

        LoginPage loginPage = new LoginPage();
        loginPage.setUsername(user.username);
        loginPage.setPassword(user.password);
        loginPage.clickSignInBtn();
        HomePage homePage = new HomePage();

        Assert.assertTrue(homePage.isGetYourFeedTabTitleDisplayed());
    }

    @Test
    public void verifyUserCanLogOut() {
        User user = new User();
        user.setToUser(UserType.BASE);

        LoginPage loginPage = new LoginPage();
        loginPage.setUsername(user.username);
        loginPage.setPassword(user.password);
        loginPage.clickSignInBtn();
        HomePage homePage = new HomePage();
        homePage.header.clickSettingsTab();
        SettingsPage settingsPage = new SettingsPage();
        settingsPage.clickLogoutBtn();
        settingsPage.header.clickSignInTab();
        loginPage = new LoginPage();

        Assert.assertTrue(loginPage.isLoginPage());
    }

    //TODO we should add negative scenarios here. Verifying that user with wrong and/or lacking credentials can't
    // enter the app. Also if we have AC - add validations on the error messages that appear.
}
