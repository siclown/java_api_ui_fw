package UISpecs;

import Base.BaseUISpec;
import Enums.UserType;
import PageObjects.ArticlePage;
import PageObjects.HomePage;
import PageObjects.LoginPage;
import PageObjects.NewPostPage;
import Utils.StringUtils;
import Utils.User;
import org.junit.Assert;
import org.junit.Test;

public class NewPostSpec extends BaseUISpec {

    @Test
    public void verifyUserCanCreateNewPosts() {
        User user = new User();
        user.setToUser(UserType.BASE);
        String timestamp = StringUtils.timestamp();
        String title = "autoTitle" + timestamp;
        String subject = "autoSubject" + timestamp;
        String description = "autoDescription" + timestamp;

        LoginPage loginPage = new LoginPage();
        loginPage.setUsername(user.username);
        loginPage.setPassword(user.password);
        loginPage.clickSignInBtn();
        HomePage homePage = new HomePage();
        homePage.header.clickNewPostTab();
        NewPostPage newPostPage = new NewPostPage();
        newPostPage.setArticleTitle(title);
        newPostPage.setWhatsThisAbout(subject);
        newPostPage.setWriteYourArticle(description);
        newPostPage.clickPublishArticleBtn();
        ArticlePage articlePage = new ArticlePage();
        //TODO for a full scenario we can verify elements in article page -title, subject, description
        articlePage.header.clickHomeTab();
        homePage = new HomePage();
        homePage.clickOnGlobalFeedTab();

        Assert.assertEquals(title, homePage.getFirstArticleTitleValue(user.name));
        Assert.assertEquals(subject, homePage.getFirstArticleSubjectValue(user.name));
        //TODO for a full scenario we can add here that clicking on the post takes us to article page
    }

    //TODO if we have clear AC we can add tests for verifying restrictions when creating new posts - accepted symbols
    // in fields, symbol number limits, error messages, etc.
}
